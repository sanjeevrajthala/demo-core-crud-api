﻿using core_api.Contexts;
using core_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace core_api.Repositories
{
    public class StudentRepository: IStudentRepository
    {
        #region Declared Variables
        private readonly ApplicationDbContext _context;

        #endregion

        #region Constructor
        public StudentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        #endregion

        #region Get all students
        public IEnumerable<Student> GetAllStudents()
        {
            return _context.Students.ToList();
        }
        #endregion

        #region Get student by id

        public Student GetStudentById(int id)
        {
            return _context.Students.FirstOrDefault(p => p.Id == id);
        }
        #endregion

        #region Create Student

        public void CreateStudent(Student student)
        {
            if (student == null) throw new ArgumentNullException(nameof(student));

            _context.Students.Add(student);
        }
        #endregion

        #region Update Student
        public void UpdateStudent(Student student)
        {
        }
        #endregion

        #region Delete Student

        public void DeleteStudent(Student student)
        {
            if (student == null) throw new ArgumentNullException(nameof(student));

            _context.Students.Remove(student);
        }
        #endregion

        #region Save db
        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }
        #endregion
    }
}
