﻿using core_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace core_api
{
    public interface IStudentRepository
    {
        IEnumerable<Student> GetAllStudents();

        Student GetStudentById(int id);

        void CreateStudent(Student student);

        void UpdateStudent(Student student);

        void DeleteStudent(Student student);

        bool SaveChanges();
    }
}
