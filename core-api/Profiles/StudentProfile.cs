﻿using AutoMapper;
using core_api.Dtos;
using core_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace core_api.Profiles
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {

            CreateMap<Student, StudentReadDto>();

            CreateMap<StudentCreateDto, Student>()
                .AfterMap((src, dest) =>
                {
                    dest.CreatedDate = DateTime.Now;
                });

            CreateMap<StudentUpdateDto, Student>()
                .AfterMap((src, dest) =>
                {
                    dest.ModifiedDate = DateTime.Now;
                });
        }
    }
}
