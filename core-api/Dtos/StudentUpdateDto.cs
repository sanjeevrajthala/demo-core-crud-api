﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace core_api.Dtos
{
    public class StudentUpdateDto : StudentCreateDto
    {
        public bool IsActive { get; set; }
    }
}
