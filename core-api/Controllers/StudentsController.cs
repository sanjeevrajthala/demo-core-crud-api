﻿using AutoMapper;
using core_api.Dtos;
using core_api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace core_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        #region Declared Variables
        private readonly IStudentRepository _repository;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor
        public StudentsController(IStudentRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        #endregion

        #region Get All Students
        //GET api/students
        [HttpGet]
        public ActionResult<IEnumerable<StudentReadDto>> GetAllStudents()
        {
            var students = _repository.GetAllStudents();
            return Ok(_mapper.Map<IEnumerable<StudentReadDto>>(students));
        }
        #endregion

        #region Get Student By Id
        //GET api/students/{id}
        [HttpGet("{id}", Name = "GetStudentById")]
        public ActionResult<StudentReadDto> GetStudentById(int id)
        {
            var student = _repository.GetStudentById(id);

            if (student != null)
            {
                return Ok(_mapper.Map<StudentReadDto>(student));

            }
            return NotFound();
        }
        #endregion

        #region Create Student
        //POST api/students
        [HttpPost]
        public ActionResult<StudentReadDto> CreateStudent(StudentCreateDto studentCreateDto)
        {
            var student = _mapper.Map<Student>(studentCreateDto);

            _repository.CreateStudent(student);
            _repository.SaveChanges();

            var studentReadDto = _mapper.Map<StudentReadDto>(student);

            return CreatedAtRoute(nameof(GetStudentById), new { Id = studentReadDto.Id }, studentReadDto);
        }
        #endregion

        #region Update Student
        //PUT api/students/1
        [HttpPut("{id}")]
        public ActionResult UpdateStudent(int id, StudentUpdateDto studentUpdateDto)
        {
            var studentFromRepo = _repository.GetStudentById(id);

            if (studentFromRepo == null) return NotFound();

            _mapper.Map(studentUpdateDto, studentFromRepo);

            _repository.UpdateStudent(studentFromRepo);
            _repository.SaveChanges();

            return NoContent();
        }
        #endregion

        #region Delete Student
        //DELETE api/students/1
        [HttpDelete("{id}")]
        public ActionResult DeleteStudent(int id)
        {
            var studentFromRepo = _repository.GetStudentById(id);

            if (studentFromRepo == null) return NotFound();

            _repository.DeleteStudent(studentFromRepo);
            _repository.SaveChanges();

            return NoContent();
        }
        #endregion
    }
}
